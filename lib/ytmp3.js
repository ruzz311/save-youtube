// See https://github.com/tobilg/youtube-mp3-downloader
const YoutubeMp3Downloader = require('youtube-mp3-downloader');
let nodeID3 = require('node-id3');

function Downloader(options={}) {
    const self = this;
    const opts = Object.assign({
        "ffmpegPath": "/usr/local/bin/ffmpeg",   // Where is the FFmpeg binary located?
        "outputPath": `${__dirname}/downloads`,  // Where should the downloaded and encoded files be stored?
        "youtubeVideoQuality": "highest",        // What video quality should be used?
        "queueParallelism": 2,                   // How many parallel downloads/encodes should be started?
        "progressTimeout": 2000                  // How long should be the interval of the progress reports
    }, options);

    //Configure YoutubeMp3Downloader with your settings
    self.YD = new YoutubeMp3Downloader(opts);

    self.callbacks = {};

    self.YD.on("finished", function (error, data) {
        if (self.callbacks[data.videoId]) {
            self.callbacks[data.videoId](error, data);
        } else {
            console.log("Error: No callback for videoId!");
        }
    });

    self.YD.on("error", function (error, data) {
        console.error(`${error}`);
        console.error(`${error} on videoId ${data.videoId}`);

        if (self.callbacks[data.videoId]) {
            self.callbacks[data.videoId](error, data);
        } else {
            console.log("Error: No callback for videoId!");
        }
    });
}

Downloader.prototype.getMP3 = function (track, callback) {
    let self = this;
    // Register callback
    self.callbacks[track.videoId] = callback;
    // Trigger download
    self.YD.download(track.videoId, track.name);
};


Downloader.prototype.setID3Info = function setID3Info(track, filePath) {
    if (!track.id3) {
        console.warn(`id3 info not provided for`, filePath, track)
    }
    return nodeID3.write(track.id3, filePath);
};

Downloader.prototype.getMp3Playlist = function (tracks) {
    let self = this;
    let songCount = tracks.length;
    let completed = 0;

    tracks.map((track, i) => {
        self.getMP3(track, (err, res) => {
            if (err) {
                console.error(`Error on song #${i+1}: ${track.name}`);
                throw err;
            } else {
                completed++;
                console.log(`${i+1} of ${songCount} was written to disk: ${res.file}`);
                if (self.setID3Info(track, res.file)) {
                    console.log(`ID3 track info written to mp3: ${res.file}`);
                }
            }
        });
    })
};

module.exports = Downloader;