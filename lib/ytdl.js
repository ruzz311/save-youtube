// see https://github.com/przemyslawpluta/node-youtube-dl

const path = require('path');
const fs = require('fs');
const ytdl = require('youtube-dl');

function playlist(url) {
    'use strict';
    const video = ytdl(url);

    video.on('error', function error(err) {
        console.log('error 2:', err);
    });

    let size = 0;
    video.on('info', function (info) {
        size = info.size;
        const output = path.join(`${__dirname}/${size}.mp4`);
        video.pipe(fs.createWriteStream(output));
    });

    let pos = 0;
    video.on('data', function data(chunk) {
        pos += chunk.length;
        // `size` should not be 0 here.
        if (size) {
            const percent = (pos / size * 100).toFixed(2);
            process.stdout.cursorTo(0);
            process.stdout.clearLine(1);
            process.stdout.write(percent + '%');
        }
    });

    video.on('next', playlist);
}

/**
 *
 */
function getExtractors(){
    ytdl.getExtractors(true, function (err, list) {
        console.log('Found ' + list.length + ' extractors');
        for (var i = 0; i < list.length; i++) {
            console.log(list[i]);
        }
    });
}

module.exports = {
    playlist,
    getExtractors
};

//
// EXAMPLES
//
// var ytdl = require('./lib/ytdl');
// ytdl.getExtractors();
// ytdl.playlist('https://www.youtube.com/playlist?list=PLOYToCDppDhXdVgknSx7UM-STrH9r44wb');