let Downloader = require('./lib/ytmp3');
let dl = new Downloader({
    outputPath: `${__dirname}/downloads`
});

// import/create your own!
[
    'disney-dj-shuffle-2'
].forEach(playlist => {
    let playlist = require(`./playlists/${playlist}`);
    dl.getMp3Playlist(playlist.extractTracks());
});